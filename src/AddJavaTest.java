class AddJavaTest
{
    public static void main(String args[])
    {
        //@deepalgo    
	List res1 = simpleAdd(1);
	 //@deepalgo
	 List res2 = doubleSimpleAdd(1);
	List res3 = doubleSimpleAddWithBetweenInstruction(1);
	List res4 = addWithInstruction(1);
	List res5 = doubleAdd(1);
	List res6 = addOnObject(1);
	List res7 = simpleAddReturn(1);
	int res8 = addWithInstructionInt(1);
	int res9 = addWithInstructionIntReturn(1); 						
    }

    //@deepalgo
     public static List simpleAdd(int count){
        List dbls = new ArrayList();
	dbls.add(5);
	return dbls;
    }

    //@deepalgo
    public static List doubleSimpleAdd(int count){
        List dbls = new ArrayList();
	dbls.add(5);
	dbls.add(count);
	return dbls;
    }

    //@deepalgo
    public static List doubleSimpleAddWithBetweenInstruction(int count){
        List dbls = new ArrayList();
	dbls.add(5);
	int size = dbls.size;
	dbls.add(count);
	dbls.add(size);
	return dbls;
    }

    //@deepalgo
     public static List addWithInstruction(int count){
        List dbls = new ArrayList();
	int a = dbls.add(5).size;
	return dbls;
    }

    //@deepalgo
    public static List doubleAdd(int count){
        List dbls = new ArrayList();
	dbls.add(5).add(count);
	return dbls;
    }

    //@deepalgo
    public static List addOnObject(int count){
	Person person;
	person.friends.add(5);
	return person.friends;
    }

    //@deepalgo
     public static List simpleAddReturn(int count){
        List dbls = new ArrayList();
	return dbls.add(5);
    }

    //@deepalgo
    public static int addWithInstructionInt(int count){
        List dbls = new ArrayList();
	int a = dbls.add(5).size;
	return a;
    }

    //@deepalgo
    public static int addWithInstructionIntReturn(int count){
        List dbls = new ArrayList();
	return dbls.add(5).size;
    }
}
